package tests

import (
	"testing"
	xsellco "gitlab.com/ogar.vasily/go-xsellco"
)

func TestCreateThread(t *testing.T) {
	err, _ := xsellco.CreateThread([]byte(`{"channel_name":"xsellco@gmx.com", "recipient_email":"morfij@gmail.com", "recipient_name":"Stan"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestGetThread(t *testing.T) {
	err, _ := xsellco.GetThreadById("114454508", []byte(``))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestUpdateThread(t *testing.T) {
	err, _ := xsellco.UpdateThreadById("114454508", []byte(`{"type":"Negative Feedback"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestThreadsGetCustomFields(t *testing.T) {
	err, _ := xsellco.GetThreadsCustomFields("114454508")

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestThreadsUpdateCustomFields(t *testing.T) {
	err, _ := xsellco.UpdateThreadsCustomFields("114454508", []byte(`{"my-custom-field":"new text"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}
