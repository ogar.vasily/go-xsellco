package tests

import (
	"testing"
	xsellco "gitlab.com/ogar.vasily/go-xsellco"
)

func TestCreateMessage(t *testing.T) {
	err, _ := xsellco.CreateMessage([]byte(`{"thread_id":"114454508", "subject":"Sample subject line", "body":"Sample body line"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestGetMessage(t *testing.T) {
	err, _ := xsellco.GetMessageById("518772633", []byte(``))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

}

func TestUpdateMessage(t *testing.T) {
	err, _ := xsellco.UpdateMessageById("518772633", []byte(`{"type":"Negative Feedback"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}

func TestListMessages(t *testing.T) {
	err, _ := xsellco.ListMessages("114454508", 2)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}
