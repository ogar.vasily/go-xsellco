package tests

import (
	"testing"
	xsellco "gitlab.com/ogar.vasily/go-xsellco"
)


func TestCreateOrder(t *testing.T) {
	err, _ := xsellco.CreateOrder([]byte(`{"channel_name":"xsellkokoko@gmx.com",
	"seller_order_id":"seller_order_id123",
	"currency":"USD",
	"item_amount":1,
    "order_items": [{"sku": "T3R32TR3",
            "title": "Sample Product Title",
            "quantity": 1,
            "item_amount": 10.00,
            "shipping_amount": 0.00
        }],
    "ship_to": {
    	"name": "John Smith",
    	"line_1": "Line 1 of Address",
    	"city": "Address City",
    	"county": "Address County",
    	"country": "IE"
    },
    "buyer_email": "john@example.com"
}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
}
