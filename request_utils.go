package go_xsellco

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"gitlab.com/ogar.vasily/go-xsellco/limiter"
	"io/ioutil"
	"fmt"
	"time"
	"path/filepath"
	"path"
	"os"
	"runtime"
	logrus "github.com/Sirupsen/logrus"

)

var api_url = "https://api.xsellco.com/v1"

func Get(endpoint string, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := get(endpoint)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func get(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("GET", url, nil)
	req.SetBasicAuth(Cred.User, Cred.Pass)

	return client.Do(req)
}

//
func Post(endpoint string, params []byte, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := post(endpoint, params)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func post(endpoint string, jsonStr []byte) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.SetBasicAuth(Cred.User, Cred.Pass)

	return client.Do(req)
}

func Put(endpoint string, params []byte, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := put(endpoint, params)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func put(endpoint string, jsonStr []byte) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	req.SetBasicAuth(Cred.User, Cred.Pass)

	return client.Do(req)
}
func Delete(endpoint string, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := delete(endpoint)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func delete(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("DELETE", url, nil)

	return client.Do(req)
}

func responseError(r http.Response) error {



	switch r.StatusCode {
	case 200, 201, 204:
		return nil
	default:

		log := logrus.New()
		log.SetFormatter(&logrus.JSONFormatter{TimestampFormat: "02-01-2006 15:04:05", PrettyPrint: true})
		log.SetReportCaller(true)

		currentTime := time.Now()
		filename := currentTime.Format("02-01-2006") + ".log"

		_, pack, _, ok := runtime.Caller(0)
		if !ok {
			panic("No caller information")
		}

		logPath := filepath.Join(path.Dir(pack), "logs")
		os.MkdirAll(logPath, os.ModePerm)

		fullLogPath := logPath + "/" + filename
		f, err := os.OpenFile(fullLogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)

		defer f.Close()
		if err != nil {
			fmt.Println(err)
		} else {
			log.SetOutput(f)
		}

		b, _ := ioutil.ReadAll(r.Body)
		errorMessage := errors.New(string(b))

		log.WithFields(logrus.Fields{"error": errorMessage}).Error("xSellco request error")

		return errorMessage
	}

}
