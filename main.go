package go_xsellco

import (
	"path/filepath"
	"io/ioutil"
	"encoding/json"
)

type Credentials struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

var Cred = new(Credentials)
var credPath string

func main() {
}

func SetCredentials(filepath string) {
	credPath = filepath
	getCredentials()
}

func getCredentials() {
	if credPath == "" {
		panic("Please set Credentials file path")
	}
	file, _ := filepath.Abs(credPath)
	b, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(b, &Cred)
}
