# API calls

## Orders
- [x] Create an order
- [x] Create an order
- [x] Retrieve an order
- [x] Update an Order
- [x] Delete an Order
- [x] List orders
- [ ] Upload tracking information

## Threads
- [x] Create a thread
- [x] Retrieve a thread
- [x] Update a thread
- [x] List threads
- [x] Get custom fields
- [x] Update custom field

## Messages

- [x] Create a message
- [x] Retrieve a message
- [x] Update a message
- [x] List messages

## Repricers

- [ ] Upload a repricer file
- [ ] Get a repricer file

## Users

- [ ] Retrieve a user
- [ ] List users

## Channels

- [ ] Retrieve a channel
- [ ] List channels