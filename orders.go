package go_xsellco

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-xsellco/models"
)

//Data Parameters
//Parameter 	Type 	Description
//channel_name 	string 	Name of the channel the order is to be bound to. The channel must be of type “Website”; that is, it cannot be, for example, of type ‘Amazon’, 'eBay’, 'Shopify’, etc. REQUIRED
//seller_order_id 	string 	Your representation of an order identifier REQUIRED
//status 	string 	If not specified, defaults to 'Pending’. Status types: 'Pending’, 'Processed’, 'Shipped’, 'Canceled’, 'Returned’ OPTIONAL
//currency 	string 	See full list; e.g., 'USD’ REQUIRED
//item_amount 	float 	Total item sales value of order REQUIRED
//shipping_amount 	float 	Total shipping value of order. Types to 0.00 OPTIONAL
//total_amount 	float 	Total item sales and shipping value of order. Types to item_amount + shipping_amount OPTIONAL
//order_items 	OrderItem 	Array of OrderItems. Requires at least a one-element array of OrderItems REQUIRED
//ship_to 	Address 	Address of the order recipient REQUIRED
//bill_to 	Address 	Address of the order bill-payer. Types to the ship_to address OPTIONAL
//email 	String 	Billing e-mail address associated with the order REQUIRED
//order_url 	String 	Your URL of the order OPTIONAL
//tracking_code 	String 	Tracking code associated with the order OPTIONAL

func CreateOrder(params []byte) (error, *models.OrdersCreateResponse) {
	data := new(models.OrdersCreateResponse)
	endpoint := fmt.Sprintf("/orders")
	err := Post(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//URL Parameters
//Parameter 	Type 	Description
//id 	integer 	Our ID representation of the order REQUIRED

func GetOrderById(id string) (error, *models.OrdersGetResponse) {
	data := new(models.OrdersGetResponse)
	endpoint := fmt.Sprintf("/orders/%s", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}


//URL Parameters
//Parameter 	Type 	Description
//id 	integer 	Our ID representation of the order, returned following a POST or GET request REQUIRED

//Data Parameters
//Parameter 	Type 	Description
//channel_name 	string 	Name of the channel the order is to be bound to. The channel must be of type “Website”; that is, it cannot be, for example, of type ‘Amazon’, 'eBay’, 'Shopify’, etc. No change if unspecified REQUIRED
//item_amount 	float 	Total item sales value of order. No change if unspecified OPTIONAL
//shipping_amount 	float 	Total shipping value of order. No change if unspecified OPTIONAL
//total_amount 	float 	Total item sales and shipping value of order. No change if unspecified OPTIONAL
//ship_to 	Address 	Address of the order recipient. No change if unspecified. Otherwise, only specified fields get updated OPTIONAL
//bill_to 	Address 	Address of the order bill-payer. No change is unspecified OPTIONAL
//currency 	string 	See full list; e.g., 'USD’ OPTIONAL
//status 	string 	Permitted values: “Pending”, “Processing”, “Shipped”, “Returned”, “Canceled”. Types to 'Pending’ OPTIONAL
//email 	string 	Billing e-mail address associated with the order OPTIONAL
//order_items 	Array of OrderItems 	All existing OrderItems associated with the order are over-written with the new OrderItems. OPTIONAL
//order_created_at 	timestamp 	No change if unspecified OPTIONAL
//delivery_type 	string 	Permitted values: 'Expedited’, 'NextDay’, 'SecondDay’, 'Standard’ (case-insensitive) OPTIONAL
//tracking_code 	string 	No change if unspecified OPTIONAL
//order_url 	string 	No change if unspecified OPTIONAL

func UpdateOrderById(id string, params []byte) (error, *models.OrdersUpdateResponse) {
	data := new(models.OrdersUpdateResponse)
	endpoint := fmt.Sprintf("/orders/%s", id)
	err := Put(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//URL Parameters
//Parameter 	Type 	Description
//id 	integer 	Order object ID REQUIRED
//Deleted orders cannot be restored


func DeleteOrderById(id string) (error, *models.OrdersDeleteResponse) {
	data := new(models.OrdersDeleteResponse)
	endpoint := fmt.Sprintf("/orders/%s", id)
	err := Delete(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//URL Parameters
//Parameter 	Type 	Description
//channel_name 	string 	Required if the seller_order_id is specified. Otherwise, it’s optional
//seller_order_id 	string 	Your representation of an order identifier OPTIONAL
//type 	string 	Required if channel_name is not specified. Otherwise, it’s optional. Permitted values: 'Amazon’, 'Ebay’, 'Magento’, 'Website’, 'Shopify’ (case-insensitive)
//ordered_after 	timestamp 	OPTIONAL
//ordered_before 	timestamp 	OPTIONAL
//status 	string 	Permitted values: 'Order Received’, 'Payment Received’, 'Payment Rejected’, 'Payment Accepted’, 'Order Shipping’, 'In Transit’, 'Delivered’, 'Canceled’, 'Returned’, and 'Hold’ (case-insensitive) OPTIONAL


func ListOrders(params []byte) (error, *models.OrdersListResponse) {
	data := new(models.OrdersListResponse)
	endpoint := fmt.Sprintf("/orders?%s", params)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
