package models

import (
	"github.com/shopspring/decimal"
	"time"
)

type Address struct {
		Name string `json:"name,omitempty"`
		Line1 string `json:"line_1,omitempty"`
		Line2 string `json:"line_2,omitempty"`
		Line3 string `json:"line_3,omitempty"`
		PhoneNumber string `json:"phone_number,omitempty"`
		Email string `json:"email,omitempty"`
		City string `json:"city,omitempty"`
		County string `json:"county,omitempty"`
		District string `json:"district,omitempty"`
		State string `json:"state,omitempty"`
		PostCode string `json:"postcode,omitempty"`
		Country string `json:"country,omitempty"`
		Latitude string `json:"latitude,omitempty"`
		Longitude string `json:"longitude,omitempty"`
}

type OrdersCreateResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data struct {
		OrderID string `json:"order_id"`
		ChannelName string `json:"channel_name"`
		SellerOrderID string `json:"seller_order_id"`
		OrderCreatedAt time.Time `json:"order_created_at"`
		Status string `json:"status"`
		Currency string `json:"currency"`
		ItemAmout  decimal.Decimal `json:"item_amount"`
		ShippingAmount decimal.Decimal `json:"shipping_amount"`
		TotalAmount decimal.Decimal `json:"total_amount"`
		OrderItems []struct {
			OrderItemID string `json:"order_item_id"`
			Sku string `json:"sku"`
			Title string `json:"title"`
			Quantity int `json:"quantity"`
			ItemAmount int  `json:"item_amount"`
			ShippingAmount decimal.Decimal `json:"shipping_amount"`
		} `json:"order_items"`
		BillTo Address `json:"bill_to,omitempty"`
		ShipTo Address `json:"ship_to"`
		Carrier string `json:"carrier"`
		TrackingCode string `json:"tracking_code"`
		DeliveryType string `json:"delivery_type"`
		OrderUrl string `json:"order_url"`
		BuyerName string `json:"buyer_name"`
		BuyerEmail string `json:"buyer_email"`
		BuyerPhone string `json:"buyer_phone"`
	} `json:"data"`
}


type OrdersUpdateResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data struct {
		ID string `json:"id"`
		ChannelName string `json:"channel_name"`
		SellerOrderID string `json:"seller_order_id"`
		OrderCreatedAt time.Time `json:"order_created_at"`
		OrderShippedAt time.Time `json:"order_shipped_at"`
		Status string `json:"status"`
		Currency string `json:"currency"`
		ItemAmout  decimal.Decimal `json:"item_amount"`
		ShippingAmount decimal.Decimal `json:"shipping_amount"`
		TotalAmount decimal.Decimal `json:"total_amount"`
		OrderItems []struct {
			OrderItemID string `json:"order_item_id"`
			Sku string `json:"sku"`
			Title string `json:"title"`
			Quantity int `json:"quantity"`
			ItemAmount int  `json:"item_amount"`
			ShippingAmount decimal.Decimal `json:"shipping_amount"`
		} `json:"order_items"`
		BillTo Address `json:"bill_to"`
		ShipTo Address `json:"ship_to"`
		Carrier string `json:"carrier"`
		TrackingCode string `json:"tracking_code"`
		DeliveryType string `json:"delivery_type"`
		OrderUrl string `json:"order_url"`
		BuyerName string `json:"buyer_name"`
		BuyerEmail string `json:"buyer_email"`
		BuyerPhone string `json:"buyer_phone"`
	} `json:"data"`
	Updated bool `json:"updated"`
	Time int `json:"time"`
}

type OrdersDeleteResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	ID string `json:"id"`
	Deleted bool `json:"deleted"`
	Time int `json:"time"`
}

type OrdersListResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Page int `json:"page"`
	PageItems int `json:"page_items"`
	HasMore bool `json:"has_more"`
	Data struct {
		ID string `json:"id"`
		ChannelName string `json:"channel_name"`
		SellerOrderID string `json:"seller_order_id"`
		OrderCreatedAt time.Time `json:"order_created_at"`
		Status string `json:"status"`
		Currency string `json:"currency"`
		ItemAmout  decimal.Decimal `json:"item_amount"`
		ShippingAmount decimal.Decimal `json:"shipping_amount"`
		TotalAmount decimal.Decimal `json:"total_amount"`
		OrderItems []struct {
			OrderItemID string `json:"order_item_id"`
			Sku string `json:"sku"`
			Title string `json:"title"`
			Quantity int `json:"quantity"`
			ItemAmount int  `json:"item_amount"`
			ShippingAmount decimal.Decimal `json:"shipping_amount"`
		} `json:"order_items"`
		BillTo *Address `json:"bill_to"`
		ShipTo *Address `json:"ship_to"`
		Carrier string `json:"carrier"`
		TrackingCode string `json:"tracking_code"`
		DeliveryType string `json:"delivery_type"`
		OrderUrl string `json:"order_url"`
		BuyerName string `json:"buyer_name"`
		BuyerEmail string `json:"buyer_email"`
		BuyerPhone string `json:"buyer_phone"`
	} `json:"data"`
	Time int `json:"time"`
}

type OrdersGetResponse struct {
	OrderID string `json:"order_id"`
	ChannelName string `json:"channel_name"`
	SellerOrderID string `json:"seller_order_id"`
	OrderCreatedAt time.Time `json:"order_created_at"`
	Status string `json:"status"`
	Currency string `json:"currency"`
	ItemAmout  decimal.Decimal `json:"item_amount,omitempty"`
	ShippingAmount decimal.Decimal `json:"shipping_amount"`
	TotalAmount decimal.Decimal `json:"total_amount"`
	OrderItems []struct {
		Sku string `json:"sku"`
		Title string `json:"title"`
		Quantity int `json:"quantity"`
		ItemAmount int  `json:"item_amount"`
		ShippingAmount decimal.Decimal `json:"shipping_amount"`
	} `json:"order_items"`
	BillTo *Address `json:"bill_to,omitempty"`
	ShipTo *Address `json:"ship_to"`
	DeliveryType string `json:"delivery_type"`
	OrderUrl string `json:"order_url"`
}

